/*
 * Written by:Harel Shetu
 *===================================================
 * This program create 5 threads, every thread choosing randomly positive
 * number and check if its prime number. If it prine the thread kills all
 * other threads and prints out the prime number.
 *===================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>
#include <signal.h>
//--------------------------constants-------------------------------------
#define NUM_THREADS 5
#define RAND 100
//-------------------------global----------------------------------------
pthread_once_t threads_init = PTHREAD_ONCE_INIT;
pthread_t all_threads[NUM_THREADS + 1]; // store the threads id
pthread_key_t thread_key;
//----------------------prototypes---------------------------------------
bool is_prime(int wanted);
void* thread_function(void * args);
void once();
//---------------------------------------------------------------------------
int main()
{

	int i;
	srand((unsigned)time(NULL));

	for (i = 0; i < NUM_THREADS; i++)
	{
		int status;
		status = pthread_create(&all_threads[i],
			NULL,
			thread_function,
			NULL);
		if (status != 0)
		{
			fputs("pthread_create failed in main", stderr);
			exit(EXIT_FAILURE);

		}
	}
	all_threads[i] = pthread_self(); // store the father

	// the father waiting for all the threads
	for (i = 0; i < NUM_THREADS; i++)
		pthread_join(all_threads[i], NULL);


	return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------
/*
	this is the thread function, the function choosing rando number and checks if it prime
	if so I'm using pthread_key to store it and I call once function
*/
void* thread_function(void * args)
{
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL); //instant cancaltion     
	printf("my name is - %d\n", pthread_self());
	if (pthread_key_create(&thread_key, NULL))
	{
		fputs("key_create failed", stderr);
		exit(EXIT_FAILURE);
	}

	while (1)
	{
		pthread_testcancel(); // creates a cancellation point
		unsigned int val = rand() % RAND; // random number between 0 - RAND-1
		if (is_prime(val))
		{
			if (pthread_setspecific(thread_key, &val))
			{
				fputs("setspecific failed", stderr);
				exit(EXIT_FAILURE);
			}
			printf("my prime is - %d\n", val);
			pthread_once(&threads_init, once);
			break;
		}


	}
	pthread_exit(NULL);

}

//--------------------------------------------------------------------------
/*
this function executed only one time (using pthread_once), the thread which execute
the function kill all other threads and prints out the prime number
*/
void once()
{
	pthread_t *killer_thread = pthread_getspecific(thread_key);
	int i;
	printf("the killer name %d\n", pthread_self());
	for (i = 0; i < NUM_THREADS + 1; i++)
	{

		if (all_threads[i] != pthread_self())
		{
			pthread_cancel(all_threads[i]);
			printf("I - %d killed - %d\n", pthread_self(), all_threads[i]);
		}

	}
	printf("prime is %d\n", *killer_thread);
}
//-------------------------------------------------------------------------
/*
this function gets number and checks if it's prime or not
*/

bool is_prime(int wanted)
{
	int i;
	if (wanted < 2)
		return false;

	for (i = 2; i <= wanted / 2; i++)
	{
		if (wanted % i == 0)
		{
			return false;
		}
	}

	return true;
}
